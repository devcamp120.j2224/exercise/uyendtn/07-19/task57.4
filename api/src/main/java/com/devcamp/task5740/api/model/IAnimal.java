package com.devcamp.task5740.api.model;

public interface IAnimal {
    public void animalSound();
    public void sleep();
}
