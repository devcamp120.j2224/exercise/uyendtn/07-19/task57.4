package com.devcamp.task5740.api.model;

public class Subject {
    String subTitle;
    int subId;
    Profesor teacher;
    
    public Subject() {
    }

    public Subject(String subTitle, int subId, Profesor teacher) {
        this.subTitle = subTitle;
        this.subId = subId;
        this.teacher = teacher;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public Profesor getTeacher() {
        return teacher;
    }

    public void setTeacher(Profesor teacher) {
        this.teacher = teacher;
    }
    
}
