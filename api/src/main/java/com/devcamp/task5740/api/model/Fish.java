package com.devcamp.task5740.api.model;

public class Fish extends Annimal {
    private int size;
    private boolean canEat;
    @Override
    public void isMammal() {
        System.out.println("Fish is not mammal!");
    }
    
    public void swim() {
        System.out.println("fish is swimming...");
    }

    public Fish(int size, boolean canEat) {
        super();
        this.size = size;
        this.canEat = canEat;
    }

    public Fish(int age, String gender, int size, boolean canEat) {
        super(age, gender);
        this.size = size;
        this.canEat = canEat;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isCanEat() {
        return canEat;
    }

    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }

    
}
