package com.devcamp.task5740.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5740.api.model.Address;
import com.devcamp.task5740.api.model.Annimal;
import com.devcamp.task5740.api.model.Duck;
import com.devcamp.task5740.api.model.Fish;
import com.devcamp.task5740.api.model.Person;
import com.devcamp.task5740.api.model.Profesor;
import com.devcamp.task5740.api.model.Student;
import com.devcamp.task5740.api.model.Subject;
import com.devcamp.task5740.api.model.Worker;
import com.devcamp.task5740.api.model.Zebra;

@RestController
public class Controller {
    /**
     * @return
     */
    @CrossOrigin
    @GetMapping("persons")

    public List<Person> listPerson(@RequestParam(name = "type", required = false) String type) {
        List<Person> lstPerson = new ArrayList<Person>();

        ArrayList<Annimal> lstAnimal = new ArrayList<Annimal>();

        Annimal duck = new Duck();
        duck.setAge(2);
        duck.setGender("male");
        ((Duck)duck).setBeakColor("yellow");

        Annimal fish = new Fish(3, "female", 3, true);

        Annimal zebra  = new Zebra(true);
        zebra.setAge(4);
        zebra.setGender("male");

        lstAnimal.add(duck);
        lstAnimal.add(fish);
        lstAnimal.add(zebra);

        

        Person professor = new Profesor();
        professor.setAge(50);
        professor.setName("John");
        professor.setGender("male");
        professor.setListPet(lstAnimal);

        Person worker1 = new Worker();
         worker1.setAge(50);
         worker1.setName("Dave");
         worker1.setGender("male");
         worker1.setListPet(lstAnimal);

        ArrayList<Subject> listSubject = new ArrayList<Subject>();
        Subject subjectMath = new Subject("Math", 1, (Profesor)professor);
        listSubject.add(subjectMath);

        
        Person student1 = new Student();
         ((Student)student1).setStudentId(1);
         ((Student)student1).setSubject(listSubject);
         student1.setAge(20);
         student1.setGender("male");
         student1.setName("John");
         student1.setListPet(lstAnimal);

        if (type.equals("student")){
            lstPerson.add(student1);
        } else if(type.equals("professor")){
            lstPerson.add(professor);
        } else if (type.equals("worker")){
            lstPerson.add(worker1);
        } else {
            lstPerson.add(student1);
            lstPerson.add(professor);
            lstPerson.add(worker1);
        }

        return lstPerson;
    }
}
