package com.devcamp.task5740.api.model;

public class Profesor extends Person implements ISchool {
    int salary;
    @Override
    void eat() {
        System.out.println("profesor is eating...");
    }

    public void teaching() {
        System.out.println("profesor is teaching...");
    }

    public Profesor() {
    }

    // public Profesor(int age, String gender, String name, Address address) {
    //    super(age, gender, name, address);
      
//}

    @Override
    public void goToSchool() {
        System.out.println("profesor goes to school");
        
    }
    
    
}
