package com.devcamp.task5740.api.model;

public class Worker extends Person {
    int salary;
    @Override
    void eat() {
        System.out.println("worker is eating...");
    }
    
    public void working() {
        System.out.println("worker is working...");
    }

    public Worker() {
    }

    // public Worker(int age, String gender, String name, Address address) {
    //     super(age, gender, name, address);
    // }

    
}
