package com.devcamp.task5740.api.model;

import java.util.ArrayList;

public abstract class Person implements IAnimal,ILive {
     protected int age;
     protected String gender;
     protected String name;
    private Address address;
     protected ArrayList<Annimal> listPet;
    
    abstract void eat();

    public void goToShop() {
        System.out.println("person go to shop");
    }

    public void play() {
        System.out.println("person is playing");
    }

    public void animalSound() {
        System.out.println("person sound ...");
    }

    public void sleep() {
        System.out.println("person is sleeping...");
    }

    public Person() {
        
    }

    
    public Person(Address address) {
        this.address = address;
    }

     public Person(int age, String gender, String name, Address address) {
         this.age = age;
         this.gender = gender;
        this.name = name;
        this.address = address;
     }

     public int getAge() {
         return age;
     }
     public void setAge(int age) {
         this.age = age;
     }

     public String getGender() {
         return gender;
     }

     public void setGender(String gender) {
         this.gender = gender;
     }

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

     public ArrayList<Annimal> getListPet() {
         return listPet;
     }

     public void setListPet(ArrayList<Annimal> listPet) {
         this.listPet = listPet;
     }



    

}
