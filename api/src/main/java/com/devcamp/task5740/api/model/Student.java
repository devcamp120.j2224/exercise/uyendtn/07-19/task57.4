package com.devcamp.task5740.api.model;

import java.util.ArrayList;

public class Student extends Person implements ISchool {
    private int studentId;
    private ArrayList<Subject> subject;

    // public Student(int age, String gender, String name, Address address, int studentId, ArrayList<Subject> subject) {
    //     super(age, gender, name, address);
    //     this.studentId = studentId;
    //     this.subject = subject;
        
    // }
    @Override
    public void goToSchool() {
       System.out.println("student goes to school");
    }

    public Student() {
    }

    
    public Student(Address address) {
        super(address);
    }

    public Student(int studentId, ArrayList<Subject> subject) {
        this.studentId = studentId;
        this.subject = subject;
    }

    @Override
    void eat() {
        System.out.println("student is eating");
    }
    
    public void doHomework() {
        System.out.println("student is doing homework...");
    }

    public ArrayList<Subject> getSubject() {
        return subject;
    }

    public void setSubject(ArrayList<Subject> subject) {
        this.subject = subject;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }


   

   

}
